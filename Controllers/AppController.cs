﻿using DutchTreat.Data;
using DutchTreat.Services;
using DutchTreat.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DutchTreat.Controllers
{
    public class AppController:Controller
    {
        private readonly IDutchRepository _dutchRepository;
        private readonly IMailService _mailService;

        public AppController(IDutchRepository dutchRepository, IMailService mailService)
        {
            _dutchRepository = dutchRepository;
            _mailService = mailService;
        }
        public IActionResult Index()
        {
            return View();
        }
        //Below will just use localhost/contact not 
        //localhost/app/contact
        [HttpGet("contact")]
        public IActionResult Contact()
        {
            return View();
        }
        [HttpPost("contact")]
        public IActionResult Contact(ContactViewModel model)
        {
            if (ModelState.IsValid)
            {
                _mailService.SendEmail("test@ak", model.Subject, model.Message);
                ViewBag.UserMessage = "Email was sent";
                ModelState.Clear();
            }
            else
            {
                //show errors;
            }
            return View();
        }
        public IActionResult About()
        {
            ViewBag.Title = "Contact Us";
            return View();
        }

        [Authorize]
        public IActionResult Shop()
        {
            var results = _dutchRepository.GetAllProducts().ToList();
            return View(results);
        }
        

    }
}
