﻿using AutoMapper;
using DutchTreat.Data.Entities;
using DutchTreat.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DutchTreat.Data
{
    public class DutchMappingProfile: Profile
    {
        public DutchMappingProfile()
        {
            CreateMap<Order, OrderViewModel>()
                .ForMember(y=>y.OrderId, o2=>o2.MapFrom(t=>t.Id))
                .ReverseMap();


            CreateMap<OrderItem, OrderItemViewModel>().ReverseMap();

            //CreateMap<OrderViewModel, Order>().ForMember(g => g.Id, t => 
            //t.MapFrom(r => r.OrderId));
        }
    }
}
