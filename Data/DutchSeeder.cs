﻿using DutchTreat.Data.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DutchTreat.Data
{
    public class DutchSeeder
    {
        private readonly IWebHostEnvironment _env;
        private readonly DutchContext _context;
        private readonly UserManager<StoreUser> userManager;

        public DutchSeeder(IWebHostEnvironment env, DutchContext context, UserManager<StoreUser> userManager)
        {
            _env = env;
            _context = context;
            this.userManager = userManager;
        }
        public async Task SeedAsync()
        {
            _context.Database.EnsureCreated();

            StoreUser user = await userManager.FindByEmailAsync("adam@test.com");
            if (user == null)
            {
                user = new StoreUser()
                {
                    FirstName = "Adam",
                    LastName = "Kopaczynski",
                    Email = "adam@test.com",
                    UserName = "adam@test.com"
                };
                var result = await userManager.CreateAsync(user, "Q1wertyu!");
                if(result != IdentityResult.Success)
                {
                    throw new InvalidOperationException("Cannot create user");
                }
            }
            if (!_context.Products.Any())
            {
                //Need to add data
                var path = Path.Combine(_env.ContentRootPath, "Data", "art.json");
                var json = File.ReadAllText(path);
                var products = Newtonsoft.Json.JsonConvert.DeserializeObject<IEnumerable<Product>>(json);
                _context.Products.AddRange(products);

                var order = _context.Orders.FirstOrDefault();

                if (order != null)
                {
                    order.User = user;
                    order.Items = new List<OrderItem>()
                    {
                        new OrderItem
                        {
                            Quantity = 5,
                            Product = products.First(),
                            UnitPrice = products.First().Price
                        }
                    };
                }

                _context.SaveChanges();
            }
        }
    }
}
