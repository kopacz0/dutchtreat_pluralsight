﻿using DutchTreat.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DutchTreat.Data
{
    public class DutchRepository : IDutchRepository
    {
        private readonly DutchContext _ctx;
        private readonly ILogger<DutchRepository> _logger;

        public DutchRepository(DutchContext ctx, ILogger<DutchRepository> logger)
        {
            _ctx = ctx;
            _logger = logger;
        }

        public void AddEntity(Order order)
        {
            _ctx.Add<Order>(order);
        }

        public IEnumerable<Order> GetAllOrders(bool includeItems)
        {
            _logger.LogInformation("Get All Orders");
            if(includeItems)
                return _ctx.Orders
                    .Include(i=>i.Items)
                    .ThenInclude(j=>j.Product)

                    .OrderBy(g => g.OrderDate).ToList();
            else
            {
                return _ctx.Orders;
            }
        }

        public IEnumerable<Order> GetAllOrdersByUser(string username, bool includeItems)
        {
            _logger.LogInformation("Get All Orders By Users");
            if (includeItems)
                return _ctx.Orders
                    .Where(g=>g.User.UserName == username)
                    .Include(i => i.Items)
                    .ThenInclude(j => j.Product)

                    .OrderBy(g => g.OrderDate).ToList();
            else
            {
                return _ctx.Orders.Where(g => g.User.UserName == username);
            }
        }

        public IEnumerable<Product> GetAllProducts()
        {
            _logger.LogInformation("Get All Products");
            return _ctx.Products.OrderBy(g => g.Title).ToList();
        }

        public Order GetOrderById(string name, int id)
        {
            _logger.LogInformation("Get All Orders");
            return _ctx.Orders.Where(x=>x.User.UserName == name)
                .Include(i => i.Items)
                .ThenInclude(j => j.Product)
                .FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Product> GetProductsByCategory(string category)
        {
            return _ctx.Products.Where(g => g.Category == category).ToList();
        }
        public bool SaveAll()
        {
            return _ctx.SaveChanges() > 0;
        }
    }
}
